using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    AudioSource audioSource;
    [SerializeField] AudioClipGroupSO PianoTilesAudioClipGroup;

    [SerializeField] Button NewGameButton;
    [SerializeField] Button QuitButton;
    public static UnityEvent NewGameButtonClickedEvent = new UnityEvent();
    public static UnityEvent QuitButtonClickedEvent = new UnityEvent();

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        NewGameButton.onClick.AddListener(NewGameButtonClickedEvent.Invoke);
        QuitButton.onClick.AddListener(QuitButtonClickedEvent.Invoke);
    }

    public void MouseHoverSound()
    {
        PianoTilesAudioClipGroup.PlayAudioOneShot(audioSource);
    }
}
