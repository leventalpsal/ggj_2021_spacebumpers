using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrewController : MonoBehaviour
{
    [SerializeField]
    string CrewName;

    //Transform FollowTarget;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Follow();
    }

    void Follow()
    {

    }

    public void StartFollowing(Transform followTarget)
    {
        Debug.Log("StartFollowing");
        // FollowTarget = followTarget;

        SphereCollider sc = transform.GetComponent<SphereCollider>();
        sc.radius = 0.5f;
        sc.isTrigger = false;

        CustomSmoothFollow csf = GetComponent<CustomSmoothFollow>();
        csf.SetTarget(followTarget);
        csf.enabled = true;
    }
}
