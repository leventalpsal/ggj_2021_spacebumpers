using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{

    [SerializeField] Text RescuedCrewText;
    [SerializeField] Text DistanceText;
    [SerializeField] Text OxygenText;
    [SerializeField] RectTransform StartTipsRect;
    [SerializeField] RectTransform GameOverRect;



    private void OnEnable()
    {
        ApplicationManager.GameFirstResumeEvent.AddListener(GameFirstResume);
        PlayerCarryController.CrewRescueUpdateEvent.AddListener(CrewRescueUpdate);
        PlayerMotionController.DistanceToShipUpdateEvent.AddListener(DistanceToShipUpdate);
        OxygenController.OxygenUpdateEvent.AddListener(OxygenUpdate);
        PlayerHealthController.PlayerDiedEvent.AddListener(PlayerDied);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GameFirstResume()
    {
        StartTipsRect.gameObject.SetActive(false);
    }

    void CrewRescueUpdate(int RescuedCrew, int TotalCrew)
    {
        RescuedCrewText.text = $"{RescuedCrew} / {TotalCrew}"; 
    }

    void OxygenUpdate(float Oxygen)
    {
        OxygenText.text = Oxygen.ToString("F0");
    }

    void DistanceToShipUpdate(float DistanceToShip)
    {
        DistanceText.text = DistanceToShip.ToString("F0");
    }

    void PlayerDied()
    {
        GameOverRect.gameObject.SetActive(true);
    }
}
