using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioEmitter : MonoBehaviour
{

    private AudioSource AudioSource;
    [SerializeField] AudioClipGroupSO AudioClipGroup;

    private void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
    }

    public void PlaySoundOneShot()
    {
        if (AudioClipGroup != null)
        {
            AudioClipGroup.PlayAudioOneShot(AudioSource);
        } else
        {
            Debug.Log("Audiosource PlayOneShot");
            AudioSource.PlayOneShot(AudioSource.clip);
        }
    }
}
