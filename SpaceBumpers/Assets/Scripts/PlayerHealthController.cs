using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerHealthController : MonoBehaviour
{
    [SerializeField]
    float PlayerFullHealth;
    [SerializeField] // delete
    float PlayerHealth;
    [SerializeField]
    float SuffocatingDamage = 10;
    public static UnityEvent PlayerDiedEvent = new UnityEvent();
    private bool PlayerAlive = true;

    [SerializeField] AudioClipGroupSO DeathACG;

    private void OnEnable()
    {
        OxygenController.PlayerSuffocatingEvent.AddListener(PlayerSuffocating);
    }

    // Start is called before the first frame update
    void Start()
    {
        PlayerHealth = PlayerFullHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayerSuffocating()
    {
        PlayerHealth = Mathf.Clamp(PlayerHealth - (SuffocatingDamage * Time.deltaTime), 0, PlayerFullHealth);
        if (PlayerHealth == 0 && PlayerAlive)
        {
            PlayerAlive = false;
            AudioManager.audioPlayUnityEvent.Invoke(DeathACG);
            PlayerDiedEvent.Invoke();
        }
    }
}
