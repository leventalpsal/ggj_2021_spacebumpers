using UnityEngine.Events;

public class FloatUnityEvent : UnityEvent<float> { }
public class IntUnityEvent : UnityEvent<int> { }
public class IntIntUnityEvent : UnityEvent<int, int> { }
public class CrewUnityEvent : UnityEvent<CrewController> { }
public class AudioPlayUnityEvent : UnityEvent<AudioClipGroupSO> { }