using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static CrewUnityEvent CrewRescuedEvent = new CrewUnityEvent();
    List<CrewController> RescuedCrewList = new List<CrewController>();

    private void OnEnable()
    {
        PlayerHealthController.PlayerDiedEvent.AddListener(PlayerDied);
        CrewRescuedEvent.AddListener(CrewRescued);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Lebug.Log("CrewRescued", RescuedCrewList.Count, "Game");
    }


    void PlayerDied()
    {
        Debug.Log("GameManager PlayerDied");
    }

    void CrewRescued(CrewController RescuedCrew)
    {
        //Debug.Log($"CrewRescued {RescuedCrew.transform.name}");
        RescuedCrewList.Add(RescuedCrew);
    }

}
