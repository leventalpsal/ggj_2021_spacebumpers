using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotionController : MonoBehaviour
{
    Animator animator;
    [SerializeField]
    [Tooltip("Velocity will be multiplied by this. The bigger this is, the sooner character will blend into running")]
    float AnimationParameterConstant = 0.2f;
    public static FloatUnityEvent ApplyThrustEvent = new FloatUnityEvent();
    public static FloatUnityEvent ApplyAngularThrustEvent = new FloatUnityEvent();
    public static FloatUnityEvent DistanceToShipUpdateEvent = new FloatUnityEvent();

    private Rigidbody rb;
    [SerializeField]
    private float AngularThrustSpeed = 1f;
    [SerializeField]
    private float ThrustSpeed = 2f;

    [SerializeField]
    Transform Ship;
    float DistanceToShip;

    [SerializeField] AudioClipGroupSO CollisionACG;

    private void OnEnable()
    {
        ApplyThrustEvent.AddListener(ApplyThrust);
        ApplyAngularThrustEvent.AddListener(ApplyAngularThrust);
        PlayerHealthController.PlayerDiedEvent.AddListener(PlayerDied);
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();
        Ship = GameObject.FindGameObjectWithTag("ShipEntrance").transform;

    }

    // Update is called once per frame
    void Update()
    {
        Lebug.Log("Velocity magnitude", rb.velocity.magnitude, "Player");
        UpdateAnimaton();
        UpdateDistanceToShip();
    }

    private void FixedUpdate()
    {
        ReadInputMotionInput();
    }

    void ReadInputMotionInput()
    {
        
        if (Input.GetAxis("Vertical") != 0f)
        {
            OxygenController.CheckThrustOxygenEvent.Invoke(Input.GetAxis("Vertical"));
            //rb.velocity += Input.GetAxis("Vertical") * transform.forward * speed;
        }
        if (Input.GetAxis("Horizontal") != 0)
        {
            OxygenController.CheckAngularThrustOxygenEvent.Invoke(Input.GetAxis("Horizontal"));
            //rb.angularVelocity += new Vector3(0, Input.GetAxis("Horizontal") * rotationSpeed, 0);
        }
    }

    void ApplyThrust(float Thrust) {
        //Debug.Log($"ApplyThrust Thrust: {Thrust}");
        rb.velocity += Thrust * transform.forward * ThrustSpeed * Time.deltaTime;
    }
    void ApplyAngularThrust(float AngularThrust)
    {
        //Debug.Log($"ApplyAngularThrust AngularThrust: {AngularThrust}");
        rb.angularVelocity += new Vector3(0, AngularThrust * AngularThrustSpeed * Time.deltaTime, 0);
    }

    void UpdateAnimaton()
    {
        animator.SetFloat("Speed", rb.velocity.magnitude * AnimationParameterConstant);
    }


    void UpdateDistanceToShip()
    {
        DistanceToShip = Vector3.Distance(transform.position, Ship.position);
        DistanceToShipUpdateEvent.Invoke(DistanceToShip);
    }

    void PlayerDied()
    {
        rb.constraints = RigidbodyConstraints.FreezeAll;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.root.CompareTag("SpaceShip"))
            AudioManager.audioPlayUnityEvent.Invoke(CollisionACG);
    }
}
