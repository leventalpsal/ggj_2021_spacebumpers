using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "Managers/BootLoader")]
public class BootLoaderSO : ScriptableObject
{

    [SerializeField] GameObject ManagersPrefab;
    private GameState GameState;
    private GameObject PrefabInstance;

    private void Awake()
    {
        Debug.Log("BootLoaderSO Awake");
    }

    private void OnEnable()
    {
        Debug.Log("BootLoaderSO OnEnable");
        SceneManager.sceneLoaded += SceneWasLoaded;
        GameState = new GameState();
        //LoadManagers();
    }

    private void OnDisable()
    {
        Debug.Log("BootLoaderSO OnDisable");
        SceneManager.sceneLoaded -= SceneWasLoaded;
        //DestroyImmediate(PrefabInstance);
    }
    private void OnDestroy()
    {
        Debug.Log("BootLoaderSO OnDestroy");
        //DestroyImmediate(PrefabInstance);
    }

    void SceneWasLoaded(Scene scene, LoadSceneMode mod)
    {
        Debug.Log("BootLoaderSO SceneWasLoaded " + scene.name);
        Debug.Log($"GameState.IsManagersLoaded {GameState.IsManagersLoaded}");
        LoadManagers(scene);
    }

    void LoadManagers(Scene scene)
    {
        if (!GameState.IsManagersLoaded)
        {
            //GameState.IsManagersLoaded = true;
            PrefabInstance = Instantiate(ManagersPrefab);
            //SceneManager.LoadScene(scene.name);
        }
        GameState.IsManagersLoaded = true;

    }

}

public class GameState {
    public bool IsManagersLoaded;
}