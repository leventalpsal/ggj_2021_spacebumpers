using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(menuName = "Data/Audio Clip Group")]
public class AudioClipGroupSO : ScriptableObject
{
    [SerializeField]
    List<AudioClip> AudioClips;
    
    [SerializeField] SequenceMode SequenceMode;

    private int AudioClipToPlay = -1;
    private int LastClipPlayed = -1;

    public void PlayAudioOneShot(AudioSource audioSource)
    {
        audioSource.PlayOneShot(GetNextClip());
    }

    private AudioClip GetNextClip()
    {
        if (AudioClips.Count == 1)
            return AudioClips[0];

        switch (SequenceMode)
        {
            case SequenceMode.Random:
                AudioClipToPlay = Random.Range(0, AudioClips.Count);
                break;

            case SequenceMode.RandomNoImmediateRepeat:
                do
                {
                    AudioClipToPlay = Random.Range(0, AudioClips.Count);
                } while (AudioClipToPlay == LastClipPlayed); 
                break;

            case SequenceMode.Sequential:
                AudioClipToPlay = (int)Mathf.Repeat(++AudioClipToPlay, AudioClips.Count);
                break;
        }

        LastClipPlayed = AudioClipToPlay;
        return AudioClips[AudioClipToPlay];
    }
}

public enum SequenceMode { Random, RandomNoImmediateRepeat, Sequential }