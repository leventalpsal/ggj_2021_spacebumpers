using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCarryController : MonoBehaviour
{
    public static IntIntUnityEvent CrewRescueUpdateEvent = new IntIntUnityEvent();
    public static IntUnityEvent CrewCarryUpdateEvent = new IntUnityEvent();


    Rigidbody rb;
    bool isCarrying;

    #region Check use, if safe then remove
    [SerializeField]
    Transform CarryingWrapper;

    [SerializeField]
    Transform CrewWrapper;
    #endregion

    [SerializeField] AudioClipGroupSO TetherCrewMateACG;
    [SerializeField] AudioClipGroupSO CrewMateReturnedACG;

    //List<CrewController> AllCrew;
    CrewController[] AllCrewArray;
    int TotalCrew;

    // This is like the last link in the chain. If we are carrying multiple crew, this is 
    Transform TargetToFollowForNewCrew;
    int CarryingCrewCount;
    List<CrewController> CarriedCrew = new List<CrewController>();

    int RescuedCrewCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        CarryingCrewCount = 0;
        TargetToFollowForNewCrew = transform;

        AllCrewArray = FindObjectsOfType<CrewController>();
        TotalCrew = AllCrewArray.Length;

        CrewRescueUpdateEvent.Invoke(RescuedCrewCount, TotalCrew);
    }

    // Update is called once per frame
    void Update()
    {
        Lebug.Log("CarriedCrew List count", CarriedCrew.Count, "Player");
        Lebug.Log("CarryingCrewCount", CarryingCrewCount, "Player");
    }


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log($"Player OnTriggerEnter {other.gameObject.name}");
        if (null != other.transform.GetComponent<CrewController>())
        {
            CrewController cc = other.transform.GetComponent<CrewController>();
            CarriedCrew.Add(cc);
            cc.StartFollowing(TargetToFollowForNewCrew);
            TargetToFollowForNewCrew = other.transform;
            CarryingCrewCount++;

            AudioManager.audioPlayUnityEvent.Invoke(TetherCrewMateACG);

            //// TODO we may create an elastic joint between the two. 
            ////other.transform.parent = CarryingWrapper;
            //SphereCollider sc = other.transform.GetComponent<SphereCollider>();
            //sc.radius = 0.5f;
            //sc.isTrigger = false;

            //ConfigurableJoint cj = other.gameObject.AddComponent<ConfigurableJoint>();
            //cj.connectedBody = rb;

            // This didn't go well... Should play it more if we want to make it work good.

        }
        else if (other.transform.CompareTag("ShipEntrance") )
        {
            Debug.Log("Found Ship Entrance");
            if (CarryingCrewCount > 0)
                AudioManager.audioPlayUnityEvent.Invoke(CrewMateReturnedACG);

            for (int i = CarriedCrew.Count - 1; i >= 0; i--)
            {
                GameManager.CrewRescuedEvent.Invoke(CarriedCrew[i]);
                Destroy(CarriedCrew[i].gameObject);
            }
            RescuedCrewCount += CarryingCrewCount;
            CrewRescueUpdateEvent.Invoke(RescuedCrewCount, TotalCrew);

            CarriedCrew.Clear();
            CarryingCrewCount = 0;
            TargetToFollowForNewCrew = transform;
        }

        CrewCarryUpdateEvent.Invoke(CarryingCrewCount);
    }
}
