using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseHoverSound : MonoBehaviour, IPointerEnterHandler
{
    private MainMenuController mmc;
    // Start is called before the first frame update
    void Start()
    {
        mmc = GetComponentInParent<MainMenuController>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mmc.MouseHoverSound();
    }
}
