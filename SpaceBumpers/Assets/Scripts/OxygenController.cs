using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OxygenController : MonoBehaviour
{
    public static FloatUnityEvent CheckThrustOxygenEvent = new FloatUnityEvent();
    public static FloatUnityEvent CheckAngularThrustOxygenEvent = new FloatUnityEvent();
    public static UnityEvent PlayerSuffocatingEvent = new UnityEvent();
    
    public static FloatUnityEvent OxygenUpdateEvent = new FloatUnityEvent();

    [SerializeField]
    float OxygenTankCapacity;
    [SerializeField] // delete
    float CurrentOxygen;

    #region Oxygen consumption and refill rates
    // Rates are Oxygen Unit per second
    [SerializeField]
    float BreathingConsumption; 
    [SerializeField]
    float MotionConsumption;
    [SerializeField]
    float RotationConsumption;
    [SerializeField]
    float RefillRate;
    #endregion

    private void OnEnable()
    {
        CheckThrustOxygenEvent.AddListener(CheckThrustOxygen);
        CheckAngularThrustOxygenEvent.AddListener(CheckAngularThrustOxygen);
    }

    // Start is called before the first frame update
    void Start()
    {
        CurrentOxygen = OxygenTankCapacity;
    }

    // Update is called once per frame
    void Update()
    {
        CheckBreathingOxygen();
        OxygenUpdateEvent.Invoke(CurrentOxygen);
    }

    void CheckBreathingOxygen()
    {
        float RequiredOxygen = BreathingConsumption * Time.deltaTime;
        if (CurrentOxygen >= RequiredOxygen)
        {
            CurrentOxygen -= RequiredOxygen;
        } else
        {
            PlayerSuffocatingEvent.Invoke();
        }
    }

    void CheckThrustOxygen(float Thrust)
    {
        float RequiredOxygen = Mathf.Abs(Thrust) * MotionConsumption * Time.deltaTime;
        //Debug.Log($"CheckThrustOxygen Thrust: {Thrust} RequiredOxygen: {RequiredOxygen} CurrentOxygen: {CurrentOxygen} ");

        if (CurrentOxygen >= RequiredOxygen)
        {
            CurrentOxygen -= RequiredOxygen;
            PlayerMotionController.ApplyThrustEvent.Invoke(Thrust);
        }
    }

    void CheckAngularThrustOxygen(float AngularThrust)
    {
        float RequiredOxygen = Mathf.Abs(AngularThrust) * RotationConsumption * Time.deltaTime;
        //Debug.Log($"CheckAngularThrustOxygen AngularThrust: {AngularThrust} RequiredOxygen: {RequiredOxygen} CurrentOxygen: {CurrentOxygen} ");

        if (CurrentOxygen >= RequiredOxygen)
        {
            CurrentOxygen -= RequiredOxygen;
            PlayerMotionController.ApplyAngularThrustEvent.Invoke(AngularThrust);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.CompareTag("ShipEntrance"))
        {
            CurrentOxygen = Mathf.Min(CurrentOxygen + (RefillRate * Time.deltaTime), OxygenTankCapacity);
        }
    }
}
