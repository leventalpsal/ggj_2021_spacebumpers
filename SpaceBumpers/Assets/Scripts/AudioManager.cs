using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public static AudioPlayUnityEvent audioPlayUnityEvent = new AudioPlayUnityEvent();

    string ExPrmVolMaster = "VolMaster";
    string ExPrmVolMusic = "VolMusic";
    string ExPrmVolSFX = "VolSFX";
    string ExPrmVolGamePlayMusic = "VolGameplayMusic";
    string ExPrmVolMenuMusic = "VolMenuMusic";
    string ExPrmVolPulsatingBass = "VolPulsatingBass";
    string ExPrmVolShepardTone = "VolShepardTone";

    [SerializeField] AudioMixer AudioMixer;
    [SerializeField] AudioSource MusicGameplay;
    [SerializeField] AudioSource MusicMenu;
    [SerializeField] AudioSource MusicPulsatingBass;
    [SerializeField] AudioSource MusicShepardTone;
    
    [SerializeField] AudioSource CommonSFX;

    [SerializeField] AudioEmitter RadioChatter;
    [SerializeField] AudioEmitter Breathing;
    [SerializeField] AudioEmitter LowOxygen;
    [SerializeField] AudioEmitter JetPackLoop;

    // TODO, maybe set a better system for shephard tone volume adjusting.
    [Tooltip("Set the oxygen level where shephard tone will start playing")]
    [SerializeField] float ShepardToneOxygenThreshold = 500;
    //[Tooltip("Set the shephard volume at threshold")]
    //[SerializeField] float ShepardToneStartVolume;
    private bool PulsatingBassPlaying = false;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneWasLoaded;
        OxygenController.OxygenUpdateEvent.AddListener(UpdateShepardTone);
        PlayerCarryController.CrewCarryUpdateEvent.AddListener(UpdatePulsatingBass);
        audioPlayUnityEvent.AddListener(PlayCommonSFX);
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneWasLoaded;
        OxygenController.OxygenUpdateEvent.RemoveListener(UpdateShepardTone);
        PlayerCarryController.CrewCarryUpdateEvent.RemoveListener(UpdatePulsatingBass);
        audioPlayUnityEvent.RemoveListener(PlayCommonSFX);
    }

    // Start is called before the first frame update
    void Start()
    {
        ChangeMusicForScene(SceneManager.GetActiveScene().name);
    }

    void SceneWasLoaded(Scene scene, LoadSceneMode mod)
    {
        ChangeMusicForScene(scene.name);
    }

    void ChangeMusicForScene(string sceneName)
    {
        if (sceneName == "MainMenu")
        {
            MusicMenu.Play();
            //AudioMixer.SetFloat(ExPrmVolMenuMusic, -80f);
            StartCoroutine(FadeMixerGroup.StartFade(AudioMixer, ExPrmVolMenuMusic, 6f, 1f));
            StartCoroutine(FadeMixerGroup.StartFade(AudioMixer, ExPrmVolGamePlayMusic, 6f, 0f));
            
            // If we are returning to main menu from the game scene, Stop all other conditional effects / music 
            AudioMixer.SetFloat(ExPrmVolShepardTone, -80);
            AudioMixer.SetFloat(ExPrmVolPulsatingBass, -80);
            //StartCoroutine(FadeMixerGroup.StartFade(AudioMixer, ExPrmVolShepardTone, 1f, 0f));
            MusicPulsatingBass.Stop();
            MusicShepardTone.Stop();
            //StartCoroutine(FadeMixerGroup.StartFade(AudioMixer, ExPrmVolPulsatingBass, 1f, 0f));
            PulsatingBassPlaying = false;
        }
        else if (sceneName == "GameScene")
        {
            MusicGameplay.Play();
            MusicPulsatingBass.Play();
            //AudioMixer.SetFloat(ExPrmVolGamePlayMusic, -80f);
            StartCoroutine(FadeMixerGroup.StartFade(AudioMixer, ExPrmVolMenuMusic, 6f, 0f));
            StartCoroutine(FadeMixerGroup.StartFade(AudioMixer, ExPrmVolGamePlayMusic, 6f, 1f));
        }
    }

    void UpdateShepardTone(float oxygen)
    {
        if (oxygen <= ShepardToneOxygenThreshold)
        {
            // Max oxygen = 1000
            // ShepardToneOxygenThreshold = 300 ,below this ShepardTone will start.
            // Ratio is 0 at Oxy 300, 1 at Oxy 0
            float ratio = 1 - (oxygen / ShepardToneOxygenThreshold);
            AudioMixer.SetFloat(ExPrmVolShepardTone, Mathf.Log10(ratio) * 20 );

            if (!MusicShepardTone.isPlaying)
            {
                MusicShepardTone.Play();
            }
        } 
        else if (MusicShepardTone.isPlaying)
        {
            MusicShepardTone.Stop();
        }
    }

    void UpdatePulsatingBass(int CarriedCrewCount)
    {
        if (CarriedCrewCount > 0 && !PulsatingBassPlaying)
        {
            PulsatingBassPlaying = true;
            StartCoroutine(FadeMixerGroup.StartFade(AudioMixer, ExPrmVolPulsatingBass, 2f, 1f));
        }
        else if (CarriedCrewCount == 0 && PulsatingBassPlaying)
        {
            PulsatingBassPlaying = false;
            StartCoroutine(FadeMixerGroup.StartFade(AudioMixer, ExPrmVolPulsatingBass, 2f, 0f));
        }
    }

    void PlayCommonSFX(AudioClipGroupSO ClipGroup)
    {
        ClipGroup.PlayAudioOneShot(CommonSFX);
    }
}
