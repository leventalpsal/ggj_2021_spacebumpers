using System.Collections;
using UnityEngine.Audio;
using UnityEngine;

public static class FadeMixerGroup
{

    public static IEnumerator StartFade(AudioMixer audioMixer, string exposedParam, float duration, float targetVolume)
    {
        float currentTime = 0;
        float currentVol;
        audioMixer.GetFloat(exposedParam, out currentVol);
        currentVol = Mathf.Pow(10, currentVol / 20);
        targetVolume = Mathf.Clamp(targetVolume, 0, 1);

        do
        {
            currentTime += Time.unscaledDeltaTime;
            float newVol = Mathf.Lerp(currentVol, targetVolume, currentTime / duration);
            audioMixer.SetFloat(exposedParam, Mathf.Log10(newVol) * 20);
            yield return null;

        } while (currentTime <= duration);

        yield break;
    }
}