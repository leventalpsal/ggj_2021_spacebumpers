﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class ApplicationManager : MonoBehaviour {

	bool gameStartPaused;
	public static UnityEvent GameFirstResumeEvent = new UnityEvent();
	//public static UnityEvent GameResume = new UnityEvent();
	//public static UnityEvent GamePause = new UnityEvent();


	void OnEnable()
	{
		MainMenuController.NewGameButtonClickedEvent.AddListener(NewGame);
		MainMenuController.QuitButtonClickedEvent.AddListener(Quit);
		SceneManager.sceneLoaded += SceneWasLoaded;
		PlayerHealthController.PlayerDiedEvent.AddListener(PlayerDied);
	}

	void OnDisable()
	{
		MainMenuController.NewGameButtonClickedEvent.RemoveListener(NewGame);
		MainMenuController.QuitButtonClickedEvent.RemoveListener(Quit);
		SceneManager.sceneLoaded -= SceneWasLoaded;
		PlayerHealthController.PlayerDiedEvent.RemoveListener(PlayerDied);
	}

	private void Start()
	{
        if (SceneManager.GetActiveScene().name == "GameScene")
        {
			GameStartPause();
		}
    }

    private void Update()
    {
        if (gameStartPaused && Input.anyKeyDown)
        {
			// Dont wait and start
			Debug.Log("gameStartPaused anyKeyDown");
			GameFirstResumeEvent.Invoke();
			ResumeGame();
		}
        if (Input.GetKeyDown(KeyCode.Escape))
        {
			SceneManager.LoadScene("MainMenu");
		}
	}

    void SceneWasLoaded(Scene scene, LoadSceneMode mod)
	{
		Debug.Log("ApplicationManager scene.name " + scene.name);
		if (scene.name == "GameScene")
        {
			GameStartPause();
		}
	}

	void GameStartPause()
    {
		gameStartPaused = true;
		Time.timeScale = 0;
	}

	void ResumeGame()
    {
		gameStartPaused = false;
		Time.timeScale = 1;
	}


	public void NewGame()
	{
		StartCoroutine(LoadSceneDelayed("GameScene", 2f));
	}

	IEnumerator LoadSceneDelayed(string sceneName, float delay)
    {
		yield return new WaitForSeconds(delay);
		SceneManager.LoadScene(sceneName);
	}

	public void MainMenu()
	{
		SceneManager.LoadScene("MainMenu");
	}

	public void FromPauseToMainMenu()
	{
		Time.timeScale = 1f;
		SceneManager.LoadScene("MainMenu");
	}

	void PlayerDied()
    {
		Debug.Log("ApplicationManager PlayerDied");
		//Time.timeScale = 0f;
		StartCoroutine(LoadSceneDelayed("MainMenu", 4f));
	}

	public void Quit () 
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}
}
